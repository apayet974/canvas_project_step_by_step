
/**
 * Je crée mon objet Game
 */
function Game()
{
	this.frame = new Frame();
	this.timer = new Timer();

	this.refresh = function(ctx)
	{
		ctx.clearRect(0, 0, 1920, 1080);
	};
}

/**
 * Je crée mon objet Frame
 */
function Frame()
{
	this.frameId = 0;
	this.displayed = true;

	this.count = function()
	{
		this.frameId++;
	};
	this.display = function(ctx)
	{
		if(this.displayed)
		{
			ctx.font = "20px Sans_Serif";
			ctx.fillStyle = "#CBFFA0";
			ctx.fillText(("FrameId : " + this.frameId), 1500, 50); 
		}
	};
}

/**
 * Je crée mon objet Timer
 */
function Timer()
{
	this.displayed = true;
	this.display = function(ctx)
	{
		if(this.displayed)
		{
			ctx.font = "20px Sans_Serif";
			ctx.fillStyle = "#CBFFA0";
			ctx.fillText("Temps : " + parseInt(performance.now() / 1000), 1500, 80);
		}
	};
}

/**
 * Je crée le canvas grace à init
 */

function init()
{
	var canvas;
	var ctx;

	canvas = document.createElement('canvas');
	canvas.height = 1080;
	canvas.width = 1920;
	canvas.id = 'canvas';
	canvas.style.border = "1px solid black";
	canvas.style.display = "block";
	canvas.style.backgroundImage = "url('img/fond.png')";
	document.body.appendChild(canvas);
	ctx = canvas.getContext('2d');
	main(canvas, ctx);
}

/**
 * Je crée la fonction principale
 */
function main(canvas, ctx)
{
	var game = new Game();

	render();
	function update(game)
	{
		game.frame.count();

		return game;
	}

	function render()
	{
		game.refresh(ctx);
		game = update(game);
		game.frame.display(ctx);
		game.timer.display(ctx);

		window.requestAnimationFrame(render);
	}
}

init();
